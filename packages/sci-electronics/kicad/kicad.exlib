# Copyright 2010-2011 Denis Dupeyron <calchan@gentoo.org>
# Copyright 2013-2019 Marvin Schmidt <marv@exherbo.org>
# Copyright 2015 Jakub Kopański <jkopanski@quivade.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kicad-99999999' from Gentoo (same author), which is:
#     Copyright 1999-2010 Gentoo Foundation

require kicad-eda
require cmake
require wxwidgets [ abi=3.1 gtk_backends=[ 3 ] ]

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="KiCad GPL PCB suite"
DESCRIPTION="
KiCad is an open source (GPL) software for the creation of electronic schematic diagrams and printed
circuit board artwork.
"
HOMEPAGE="http://www.kicad-pcb.org/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    demos [[ description = [ Install the demo and example projects ] ]]
    doc [[ description = [ Build doxygen documentation ] ]]
    simulator [[ description = [ Build the Spice based schematic simulator ] ]]

    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        media-libs/glew
        net-misc/curl
        sys-libs/libgomp:=
        x11-dri/glu
        x11-dri/mesa
        x11-libs/cairo[>=1.12.0]
        x11-libs/pixman:1[>=0.30]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        simulator? (
            sci-electronics/ngspice[>=26-r2] [[ note = [ First version to provide shared library ] ]]
        )
    recommendation:
        sci-electronics/kicad-footprints [[
            description = [ Component footprints ]
        ]]
        sci-electronics/kicad-symbols [[
            description = [ Schematic symbols library ]
        ]]
    suggestion:
        sci-electronics/kicad-doc [[
            description = [ User documentation and tutorials ]
        ]]
        sci-electronics/kicad-i18n [[
            description = [ Internationalization files ]
        ]]
        sci-electronics/kicad-packages3D [[
            description = [ 3D models library ]
        ]]
"

if ever is_scm; then
    DEPENDENCIES+="
        build:
            dev-libs/boost[>=1.59.0]
            dev-libs/glm[>=0.9.8]
            dev-lang/swig[>=3.0][python]
        build+run:
            dev-lang/python:=[>=3.6]
            dev-python/wxPython:=[>=4.0.0]
            sys-libs/zlib
    "
else
    MYOPTIONS+="
        python [[ description = [ Python scripting support ] ]]
    "
    DEPENDENCIES+="
        build:
            dev-libs/boost[>=1.54.0]
            dev-libs/glm[>=0.9.5.1]
            python? ( dev-lang/swig[>=3.0][python] )
        build+run:
            python? ( dev-lang/python:2.7 )
        run:
            sys-libs/zlib
    "
fi

kicad_src_prepare() {
    cmake_src_prepare

    if ! ever is_scm; then
        # obey datadir directory location
        edo sed -e 's#DESTINATION share#DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}#' \
                -i CMakeLists.txt
    fi
}

kicad_src_configure() {
    local myconf=()

    myconf+=(
        -DwxWidgets_CONFIG_EXECUTABLE=$(wxwidgets_get_config)

        -DDEFAULT_INSTALL_PATH:PATH=/usr
        -DKICAD_DATA:PATH=/usr/share/kicad

        # needs to be DEFAULT_INSTALL_PATH/share/doc/kicad/ in order for the help files
        # to be found (path is hardcoded)
        -DKICAD_DOCS:PATH=/usr/share/doc/${PN}

        -DKICAD_USE_OCC:BOOL=OFF
        -DKICAD_USE_OCE:BOOL=OFF

        $(cmake_option demos KICAD_INSTALL_DEMOS)
        $(cmake_option simulator KICAD_SPICE)
    )

    if ever is_scm; then
        myconf+=(
            -DKICAD_USE_BUNDLED_GLEW:BOOL=OFF

            -DKICAD_SCRIPTING_WXPYTHON:BOOL=ON
        )
    else
        myconf+=(
            -DBUILD_GITHUB_PLUGIN:BOOL=TRUE

            $(cmake_option python KICAD_SCRIPTING)
            $(cmake_option python KICAD_SCRIPTING_ACTION_MENU)
            $(cmake_option python KICAD_SCRIPTING_MODULES)
            # FIXME: Requires wxPython:3.0 built with gtk+:2 support
            # $(cmake_option python KICAD_SCRIPTING_WXPYTHON)
            -DKICAD_SCRIPTING_PYTHON3:BOOL=OFF
            -DKICAD_SCRIPTING_WXPYTHON:BOOL=OFF
            -DKICAD_SCRIPTING_WXPYTHON_PHOENIX:BOOL=OFF
        )
    fi

    ecmake "${myconf[@]}"
}

kicad_src_compile() {
    default
    option doc && emake doxygen-docs
}

kicad_src_install() {
    cmake_src_install

    if option doc; then
        edo pushd "${CMAKE_SOURCE}"
        edo mv Documentation/doxygen/html doxygen
        dodoc -r doxygen
        edo popd
    fi
}

