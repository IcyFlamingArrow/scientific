# Copyright (c) 2009 Yury G. Kudryashov <urkud@ya.ru>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="PARI/GP is a widely used computer algebra system designed for fast computations in number theory."
DESCRIPTION="PARI/GP is a widely used computer algebra system designed for fast computations in
number theory (factorizations, algebraic number theory, elliptic curves...), but also contains a
large number of other useful functions to compute with mathematical entities such as matrices,
polynomials, power series, algebraic numbers etc., and a lot of transcendental functions. PARI is
also available as a C library to allow for faster computations."

HOMEPAGE="http://pari.math.u-bordeaux.fr"
DOWNLOADS="${HOMEPAGE}/pub/pari/unix/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"

# FIXME: fltk, qt (does it compile against qt4?)
# FIXME: Some docs go to /usr/share/pari/doc
# FIXME: Unsorted files in /usr/share/pari/misc
MYOPTIONS="gmp X doc"

DEPENDENCIES="
    build+run:
        sys-libs/readline
        doc? ( app-text/texlive )
        gmp? ( dev-libs/gmp:= )
        X? (
            x11-libs/libX11
            x11-proto/xorgproto
        )"

src_compile() {
    if option doc; then
        emake doc
    fi

    emake gp
}

src_prepare() {
    if option !doc; then
        edo sed -e 's/install-doc //' -i config/Makefile.SH
    fi

    edo sed -e '/STRIP/d' -i config/Makefile.SH
}

src_configure() {
    local -a myconf
    myconf=(
        $(option_with gmp)
        --graphic=$(if option X; then echo "X11"; else echo "none"; fi;)
        --prefix=/usr
    )
    edo ./Configure "${myconf[@]}"
}

src_test() {
    emake dobench
}

src_test_expensive() {
    emake test-all
}


